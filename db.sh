#!/bin/bash

#download haute garonne file @ https://geoservices.ign.fr/ocsge

#install requirements fedora
dnf install postgresql-server postgresql-contrib -y
dnf install osm2pgsql -y
dnf install ogr2ogr -y
dnf install gdal -y
dnf install postgis -y
dnf install java-11-openjdk-devel -y
dnf install p7zip
yum -y install wget
wget https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm
rpm -Uvh ./dbeaver-ce-latest-stable.x86_64.rpm

#install requirements debian
#apt update 
#apt install postgresql -y
#apt install osm2pgsql -y
#apt install gdal-bin -y
#apt install postgis -y
#apt install default-jre -y
#apt install default-jdk -y
#apt install p7zip -y

#prepare directory
chmod og+X /opt
chown $USER:$USER /opt
cd /opt

#prepare postgresql database
systemctl start postgresql
systemctl enable postgresql
postgresql-setup --initdb --unit postgresql
sudo sed -i 's/host    all             all             127.0.0.1\/32            ident/host    all             all             127.0.0.1\/32            trust/' /var/lib/pgsql/data/pg_hba.conf
sudo -u postgres createdb --encoding=UTF8 --owner=postgres gis
sudo -u postgres psql gis --command='CREATE EXTENSION postgis;'
sudo -u postgres psql gis --command='CREATE EXTENSION hstore;'

#download osm pbf file
wget https://download.geofabrik.de/europe/france/midi-pyrenees-latest.osm.pbf

sudo chown postgres:postgres midi-pyrenees-latest.osm.pbf

#create database
osm2pgsql -d gis -C 53 --create --drop midi-pyrenees-latest.osm.pbf -p osm -E 4326 --slim
sudo -i -u postgres bash << EOF
psql -d gis -c 'ALTER TABLE osm_polygon ADD geom VARCHAR;UPDATE osm_polygon SET geom = ST_AsText(ST_Transform(way, 27572));'
psql -d gis -c "create table hautegaronne as (SELECT * FROM osm_polygon WHERE name = 'Haute-Garonne')"
psql -d gis -c "create table communemontastruc as (SELECT * FROM osm_polygon op WHERE name='Montastruc-la-Conseillère' AND admin_level='8')"
psql -d gis -c "create table communetoulouse as (SELECT * FROM osm_polygon op WHERE name='Toulouse' AND admin_level='8')"
psql -d gis -c "create table communecarbonne as (SELECT * FROM osm_polygon op WHERE name='Carbonne' AND admin_level='8')"
psql -d gis -c "create table communebaren as (SELECT * FROM osm_polygon op WHERE name='Baren' AND admin_level='8')"
EOF

###script 2
7za e OCS_GE_1-1_2013_SHP_LAMB93_D031_2018-03-12.7z 
ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=postgres" OCCUPATION_SOL.shp
#si extraction dans les directory
#ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=user password=pw" OCS_GE_1-1_2013_SHP_LAMB93_D031_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D31-2013/OCCUPATION_SOL.shp 

#departement building table
sudo -i -u postgres bash << EOF
psql -d gis -c 'create table hautegaronneb as (SELECT osm.osm_id, osm.name, osm.building, osm.geom FROM osm_polygon osm JOIN hautegaronne h ON ST_contains(ST_GeometryFromText(h.geom,27572), ST_GeometryFromText(osm.geom,27572)));'
EOF


#commune buildings table 1/2
sudo -i -u postgres bash << EOF
psql -d gis -c 'create table communemontastrucb as (SELECT osm.osm_id, osm.name, osm.building, osm.geom FROM osm_polygon osm JOIN communemontastruc m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(osm.geom,27572)));'
EOF
sudo -i -u postgres bash << EOF
psql -d gis -c 'create table communetoulouseb as (SELECT osm.osm_id, osm.name, osm.building, osm.geom FROM osm_polygon osm JOIN communetoulouse m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(osm.geom,27572)));'
EOF
sudo -i -u postgres bash << EOF
psql -d gis -c 'create table communecarbonneb as (SELECT osm.osm_id, osm.name, osm.building, osm.geom FROM osm_polygon osm JOIN communecarbonne m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(osm.geom,27572)));'
EOF
sudo -i -u postgres bash << EOF
psql -d gis -c 'create table communebarenb as (SELECT osm.osm_id, osm.name, osm.building, osm.geom FROM osm_polygon osm JOIN communebaren m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(osm.geom,27572)));'
EOF
#commune sol table 
sudo -i -u postgres bash << EOF
psql -d gis -c 'ALTER TABLE occupation_sol ADD geom VARCHAR;'
psql -d gis -c 'UPDATE occupation_sol SET geom_ = ST_AsText(wkb_geometry);'
psql -d gis -c 'create table solcommontastruc as (SELECT sol.ogc_fid,sol.id,sol.code_cs,sol.millesime,sol.source,sol.geom_ FROM occupation_sol sol JOIN communemontastruc m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(sol.geom_,27572)));'
psql -d gis -c 'create table solcomtoulouse as (SELECT sol.ogc_fid,sol.id,sol.code_cs,sol.millesime,sol.source,sol.geom_ FROM occupation_sol sol JOIN communetoulouse m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(sol.geom_,27572)));'
psql -d gis -c 'create table solcomcarbonne as (SELECT sol.ogc_fid,sol.id,sol.code_cs,sol.millesime,sol.source,sol.geom_ FROM occupation_sol sol JOIN communecarbonne m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(sol.geom_,27572)));'
psql -d gis -c 'create table solcombaren as (SELECT sol.ogc_fid,sol.id,sol.code_cs,sol.millesime,sol.source,sol.geom_ FROM occupation_sol sol JOIN communebaren m ON ST_contains(ST_GeometryFromText(m.geom,27572), ST_GeometryFromText(sol.geom_,27572)));'
EOF

#extract departement buildings from gis database
sudo -i -u postgres bash << EOF
psql -d gis -A --command "SELECT osm_id, name, building as type, st_astext(geom) as geometry FROM hautegaronneb WHERE building IS NOT NULL" --dbname gis --username postgres --output departementb.csv --field-separator '#'
EOF

#extract commune buildings from gis database
sudo -i -u postgres bash << EOF

psql -A --command "SELECT osm_id, name, building as type, st_astext(geom) as geometry FROM communetoulouseb WHERE building IS NOT NULL" --dbname gis --username postgres --output communetoulouseb.csv --field-separator '#'
psql -A --command "SELECT osm_id, name, building as type, st_astext(geom) as geometry FROM communecarbonneb WHERE building IS NOT NULL" --dbname gis --username postgres --output communecarbonneb.csv --field-separator '#'
psql -A --command "SELECT osm_id, name, building as type, st_astext(geom) as geometry FROM communemontastrucb WHERE building IS NOT NULL" --dbname gis --username postgres --output communemontastrucb.csv --field-separator '#'
psql -A --command "SELECT osm_id, name, building as type, st_astext(geom) as geometry FROM communebarenb WHERE building IS NOT NULL" --dbname gis --username postgres --output communebarenb.csv --field-separator '#'
EOF

#extract hautegaronne sol from gis database
sudo -i -u postgres bash << EOF
psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, st_astext(wkb_geometry) as geometry FROM occupation_sol WHERE code_cs in ('CS2.2.1')" --dbname gis --username postgres --output solhautegaronne.csv --field-separator '#'
EOF

#extract commune sol from gis database
sudo -i -u postgres bash << EOF
psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, geom_ as geometry FROM solcomtoulouse WHERE code_cs in ('CS2.2.1')" --dbname gis --username postgres --output solcomtoulouse.csv --field-separator '#'
psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, geom_ as geometry FROM solcomcarbonne WHERE code_cs in ('CS2.2.1')" --dbname gis --username postgres --output solcomcarbonne.csv --field-separator '#'
psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, geom_ as geometry FROM solcommontastruc WHERE code_cs in ('CS2.2.1')" --dbname gis --username postgres --output solcommontastruc.csv --field-separator '#'
psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, geom_ as geometry FROM solcombaren WHERE code_cs in ('CS2.2.1')" --dbname gis --username postgres --output solcombaren.csv --field-separator '#'
EOF

chmod og-X /opt
