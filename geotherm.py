#!/usr/bin/python3
# coding: utf-8
"""
this is geotherm.py
"""
import argparse
import scipy.spatial
import pandas as pd
import numpy as np
import shapely# pylint: disable=unused-import
from planning import Planning


def process_data(csv):
    "process csv to a dataframe and returns it"
    dfproc = pd.read_csv(csv, sep="#")
    dfproc = dfproc.astype('string')
    dfproc = dfproc.drop(len(dfproc) - 1)

    def coord(row):
        row['coordpoints'] = Planning.deccoord(row['geometry'])
        #print(row)
        return row

    def surf(row):
        row['surf'] = Planning.fonction(row['coordpoints'])
        #print(row)
        return row

    def centro(row):
        row['centro'] = Planning.centroide(row['coordpoints'])
        #print(row)
        return row

    dfproc = dfproc.apply(coord, axis=1)
    dfproc = dfproc.apply(surf, axis=1)
    dfproc = dfproc.apply(centro, axis=1)
    dfproc['geot'] = 'no'
    #hypothese 6 sur base exemple rural
    dfproc['valeurseuil'] = 6 * ((dfproc['surf'] / 3.141592653589793)**(1 / 2))
    return dfproc


def process_j(dfo, dfb, i, j):
    #code à améliorer
    "process dfb and dfo in geot function to append 'oui' geot installable on building i and occsol j"
    #handmade 2 points version
    #compteur = 0
    #temp = []
    #for point in dfo.loc[j, 'coordpoints']:
    #    dist = Planning.distancepoint([dfb.loc[i, 'centro'], point])
    #    temp.append(dist)
    #for dist in temp:
    #    if dist < dfb.loc[i, 'valeurseuil']:
    #        compteur += 1
    #if compteur >= 2 and dfo.loc[j, 'surf'] >= 1.5 * dfb.loc[i, 'surf']:
    # on enleve du potentiel à hypothese 1:1.5
    #    dfo.loc[j, 'surf'] -= 1.5 * dfb.loc[i, 'surf']
    #    dfb.loc[i, 'geot'] = 'yes'

    #custom version:
    #try/except pr erreur de geometrie
    err = 0
    yes = False
    try:
        print (dfo.loc[j,'ogc_fid'])
        print(dfb.loc[i, 'osm_id'])
        p1 = Planning.interpolatepolygon(dfo.loc[j, 'coordpoints'])
        p2 = Planning.create_circle(dfb.loc[i, 'centro'],
                                    dfb.loc[i, 'valeurseuil'])
        surf = Planning.fonction(Planning.polygon(p1, p2))
        print(f"surface polygone commun/surface building:{surf}/{dfb.loc[i, 'surf']}")
    except Exception as e:# pylint: disable=broad-exception-caught
        print(f'errors:{e}')
        err += 1
        surf = 0

    #shapely:
    #try/except pr erreur de geometrie
    #err = 0
    #yes=False

    #try:
    #   p1 = shapely.Polygon(list(dfo.loc[j, 'coordpoints']))
    #   p2 = shapely.Point(dfb.loc[i, 'centro']).buffer(dfb.loc[i,
    #                                                           'valeurseuil'])
    #   surf = shapely.area(shapely.intersection(p1, p2))
    #   print(f"surf:{surf}/{dfb.loc[i, 'surf']}")
    #shapely errors : probablement
    #necessité de savoir lire le
    #C...
    #except Exception as e:
    #   print(f'errors:{e}')
    #   err += 1
    #   surf = 0
    #hypothese 1:1
    #surface batiment:surface geot
    #bcp moins pr geothermie sur sondes verticales
    #probablement moins sur geothermie horizontale
    if surf >= int(dfb.loc[i, 'surf']):
        print(f'surface suffisante pr geothermie: {surf}')
        #print(f"surface building : {int(dfb.loc[i, 'surf'])}")
        dfo.loc[j, 'surf'] -= surf
        dfb.loc[i, 'geot'] = 'yes'
        yes = True
    #amelioration sur buffers de batiments proches:
    #enlever points au polygone intersecté au lieu d'uniquement
    #la valeur de surface?

    return err, yes


def geot(building, occsol):
    "return building with 'oui' on buildings with gardens"
    dfb = process_data(building)
    dfo = process_data(occsol)

    index = 0
    end = len(dfb)
    erri = 0
    for i in range(len(dfb)):
        known_xy = np.stack(dfo['centro'].values)
        tree = scipy.spatial.cKDTree(known_xy)
        query_xy = np.stack(dfb.loc[i, 'centro'])
        # pylint: disable=unused-variable
        distances, indices = tree.query(query_xy, k=3)
        errj = 0
        for j in indices:  # 1 premiers polygones triés par distance croissante
            temperror, yes = process_j(dfo, dfb, i, j)
            errj += temperror
        if errj > 0 and yes == False:
            erri += 1
        index += 1
        print(f'avancement : {index}/{end}')
    dfb.to_csv('buildingeot', sep='#', index=False)
    print(f"nombre de negatifs avec erreurs de calcul geometrique: {erri}")
    print('nombre de geot==yes dans les résultats')
    print((dfb[dfb['geot'] == 'yes']['geot']).count())
    print('% de geot==yes dans les résultats')
    print((dfb[dfb['geot'] == 'yes']['geot']).count() / (dfb['geot']).count())
    return dfb


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='python geotherm.py -b building.csv -o occsol.csv')
    parser.add_argument('-b',
                        '--building',
                        required=True,
                        metavar='building.csv',
                        type=str,
                        help='building csvfile to process')
    parser.add_argument('-o',
                        '--occsol',
                        required=True,
                        metavar='occsol.csv',
                        type=str,
                        help='land coverage.csv to process')
    args = vars(parser.parse_args())
    geot(args['building'], args['occsol'])
