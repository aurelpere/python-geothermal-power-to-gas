test-vulnerability:
	echo "\nmake test-vulnerability\n" &&\
	trivy fs --severity HIGH,CRITICAL . 

test-misconfiguration:
	echo "\nmake test-misconfiguration\n" &&\
	trivy config --severity HIGH,CRITICAL .
venv:
	python3 -m venv .venv 
install:
	.venv/bin/pip install --upgrade pip &&\
		.venv/bin/pip install -r requirements.txt
format:
	.venv/bin/yapf -ir -vv --exclude '.venv/**' --style pep8 .
	
lint:
	for i in *.py; do .venv/bin/pylint --disable=R,C -sy $$i; done
	
test:
	.venv/bin/pytest -vv --cov=. *.py --cov-report xml:reports/coverage/coverage.xml
	
communetoulouse:
	.venv/bin/python3 geotherm.py -b communetoulouseb.csv -o solcomtoulouse.csv

communemontastruc:
	.venv/bin/python3 geotherm.py -b communemontastrucb.csv -o solcommontastruc.csv

communecarbonne:
	.venv/bin/python3 geotherm.py -b communecarbonneb.csv -o solcomcarbonne.csv

communebaren:
	.venv/bin/python3 geotherm.py -b communebarenb.csv -o solcombaren.csv

echobaren:
	echo "nombre de buildings positifs visuellement: 52-6=46"

echocarbonne:
	echo "nombre de buildings positifs visuellement: comptage necessaire sur carbonne"


departement:
	.venv/bin/python3 geotherm.py -b hautegaronneb.csv -o solhautegaronne.csv

cleaning:
	rm -rf reports

all: install format lint test coverage_badge cleaning
