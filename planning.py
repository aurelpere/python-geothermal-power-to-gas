#!/usr/bin/python3
# coding: utf-8
"""
this is planning.py
"""
import math


#tuple au lieux de listes pour amelioration?
class Planning():
    "planning class made of static methods for gis calculus"

    @staticmethod
    def isdigitpoint(arg):
        "return 1 if arg is - or digit or ."
        digitspoint = {
            "-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."
        }
        for i in digitspoint:
            if str(arg) == str(i):
                return 1
        return 0

    @staticmethod
    def deccoord(chaine):
        "renvoie les points floatwkt"  # [[x1,y1],[x2,y2]...]
        listepoints = []
        doubletpoints = []
        points = ""
        doublet = 0
        index2 = 1
        for index1 in range(len(chaine)):
            if Planning.isdigitpoint(chaine[index1]):
                points += chaine[index1]
            if (Planning.isdigitpoint(chaine[index2]) == 0
                    and Planning.isdigitpoint(chaine[index1]) == 1):
                doubletpoints.append(float(points))
                doublet += 1
                points = ""
                if doublet == 2:
                    listepoints.append(tuple(doubletpoints))
                    doubletpoints = []
                    doublet = 0
            if index2 != len(chaine) - 1:
                index2 += +1
        return tuple(listepoints)

    @staticmethod
    def valeursabs(arg):
        "return absolute value of arg"
        if arg < 0:
            return -arg
        if arg >= 0:
            return +arg

    @staticmethod
    def distancepoint(pointsxy):
        "distance euclidienne,le systeme de coordonnees nest pas precise"
        if (Planning.valeursabs(pointsxy[0][0] - pointsxy[1][0]) < 100000 and
                Planning.valeursabs(pointsxy[0][1] - pointsxy[1][1]) < 100000):
            dist = ((pointsxy[1][1] - pointsxy[0][1])**2 +
                    (pointsxy[1][0] - pointsxy[0][0])**2)**(1 / 2)
        else:
            dist = 666666
        return dist

    @staticmethod
    def centroide(geompolygone):
        "return centroid of polygon geompolygone which is a list of points"
        sigmax = 0
        sigmay = 0
        for i in geompolygone:
            sigmax += i[0]
            sigmay += i[1]
        resultx = sigmax / (len(geompolygone))
        resulty = sigmay / (len(geompolygone))
        return (resultx, resulty)

    @staticmethod
    def surf(triangle):
        "methode surf triangle par determinant matriciel"
        # liste [[x1,y1],[x2,y2],[x3,y3]]
        # det:1/2[x1(y2–y3)+x2(y3–y1)+x3(y1–y2)]
        surf = (1 / 2) * (triangle[0][0] *
                          (triangle[1][1] - triangle[2][1]) + triangle[1][0] *
                          (triangle[2][1] - triangle[0][1]) + triangle[2][0] *
                          (triangle[0][1] - triangle[1][1]))
        return Planning.valeursabs(surf)

    @staticmethod
    def dedans(point, listetriangle):
        "1 si point dans 1 triangle de listetriangle"
        for triangle in listetriangle:
            surf = Planning.surf(triangle)
            surf1 = Planning.surf([point, triangle[0], triangle[1]])
            surf2 = Planning.surf([point, triangle[0], triangle[2]])
            surf3 = Planning.surf([point, triangle[1], triangle[2]])
            if surf1 + surf2 + surf3 - surf < 0.001:
                return 1

    @staticmethod
    def indexdist(point, listpoint):
        "ordonne la liste des points listepointt selon la distance à pointt"
        result0 = []
        result = []
        for pointi in listpoint:
            result0.append([pointi, Planning.distancepoint([pointi, point])])
        result0.sort(key=lambda x: x[1])
        for pointk in result0:
            result.append(pointk[0])
        return result

    @staticmethod
    def inside(point, polygon):
        "return 1 if point inside polygon"
        # https://www.youtube.com/watch?v=Fp2n9TinlOw
        # un point à l'interieur du polygone a un nombre pair d'intersection avec le segment qui le relie au barycentre du polygone
        # un point à l'exterieur du polygone a un nombre impair d'intersection avec le segment qui le relie au barycentre du polygone
        # hypothese polygone sans trou (traitement postgis préalable)
        temp = 0
        for i in range(len(polygon) -
                       1):  #-1 pour aller jusqu'a la derniere valeur à i+1
            if Planning.secant([polygon[i], polygon[i + 1]],
                               [point, Planning.centroide(polygon)]):
                #debug
                #print(f'segments : [{polygon[i]},{polygon[i+1]}],[{point},{Planning.centroide(polygon)}]')
                temp += 1
                #print (f'temp: {temp}')
                #print(f'segments : [{polygon[-1]},{polygon[0]}],[{point},{Planning.centroide(polygon)}]')
        if Planning.secant([polygon[-1], polygon[0]],
                           [point, Planning.centroide(polygon)]):
            temp += 1
            #debug
            #print (f'temp: {temp}')
            #print(f'temp final:{temp}')
        dict_ = {1: 1, 0: 0}
        return dict_[temp % 2 == 0]

    @staticmethod
    def polygon(polygon1, polygon2):
        "return overlaping polygon"
        list1 = []
        list2 = []
        for i in polygon1:
            if Planning.inside(i, polygon2):
                #print('inside polygon2')
                list1.append(i)
        for j in polygon2:
            if Planning.inside(j, polygon1):
                #print('inside polygon1')
                list2.append(j)
        #debug
        #print(f'list1:{list1}')
        #print(f'list2:{list2}')
        #print(len(list1))
        #print(len(list2))
        #result = list1 + list2
        #ameliorer algo pour probleme geometrie
        def fvide(list1,list2):# pylint: disable=unused-argument
            liste=[]
            #print('fvide')
            return liste
        def flist2(list1,list2):# pylint: disable=unused-argument
            #si polygone 2 contenu dans polygone 1
            #print('function flist2')
            return list1
        def flist1(list1,list2):# pylint: disable=unused-argument
            #si polygone 1 contenu dans polygone 2
            #print('function flist1')
            return list2
        def flist12(list1,list2):# pylint: disable=unused-argument
            #si les deux polygones ont une surface commune
            if Planning.distancepoint([list2[-1], list1[0]]) < Planning.distancepoint([list1[-1], list2[0]]):
                #print('function flist21')
                result = list2+list1
            else:
                #print('flist12')
                result= list1+list2
            return result
        dict_={(0,0):fvide,(1,0):flist2,(0,1):flist1,(1,1):flist12}
        result=dict_[(len(list2)>0,len(list1)>0)](list1,list2)
        #if (len(list2) != 0 and len(list1) != 0):
        #    if Planning.distancepoint([list2[-1], list1[0]]) < Planning.distancepoint([list1[-1], list2[0]]):
        #    else:
        #        result = list1 + list2
        #elif (len(list2) != 0 and len (list1)==0):
        #    result=list2
        #elif (len(list1) != 0 and len(list2)==0):
        #    result=list1
        #else:
        #    result = []
        return result

    @staticmethod
    def interpolate(xy):
        #verif dx et dy
        "renvoie une liste de tuple des points interpolés entre X(a,b) et Y(d,e) xy==[[a,b],[d,e]]"
        d = Planning.distancepoint(xy)
        if d <= 1:
            return xy
        elif Planning.valeursabs(xy[1][0] - xy[0][0]) != 0:
            xi = xy[0][0]
            dx = (1 / d) * Planning.valeursabs(xy[1][0] - xy[0][0])
            boolean = (xy[1][0] - xy[0][0]) / Planning.valeursabs(xy[1][0] -
                                                                  xy[0][0])
            result = [0] * (2 + (int(d) - 1))  ##verifier les limites
            result[0] = xy[0]
            for i in range(1, len(result) - 1):
                xi += boolean * dx
                yi = xy[0][1] * (xy[1][0] -
                                 xi) / (xy[1][0] - xy[0][0]) + xy[1][1] * (
                                     xi - xy[0][0]) / (xy[1][0] - xy[0][0])
                result[i] = (xi, yi)
            result[len(result) - 1] = xy[1]
        else:
            yi = xy[0][1]
            dy = (1 / d) * Planning.valeursabs(xy[1][1] - xy[0][1])
            boolean = (xy[1][1] - xy[0][1]) / Planning.valeursabs(xy[1][1] -
                                                                  xy[0][1])
            result = [0] * (2 + (int(d) - 1))  ##verifier les limites
            result[0] = xy[0]
            for i in range(1, len(result) - 1):
                yi += boolean * dy
                xi = xy[0][0] + ((yi - xy[0][1]) /
                                 (xy[1][1] - xy[0][1])) * (xy[1][0] - xy[0][0])
                result[i] = (xi, yi)
            result[len(result) - 1] = xy[1]
        return result

    @staticmethod
    def interpolate_segment(step, start, end):
        #chatgpt
        """
        Interpolates a line segment between two points (start and end) with a specified step size.
        Returns a list of points that lie on the segment at the specified intervals.
        """
        points = []
        delta_x = end[0] - start[0]
        delta_y = end[1] - start[1]
        distance = ((delta_x**2) + (delta_y**2))**0.5
        num_steps = int(distance // step)
        if num_steps == 0:
            return [start, end]
        x_step = delta_x / num_steps
        y_step = delta_y / num_steps
        for i in range(num_steps + 1):
            x = start[0] + i * x_step
            y = start[1] + i * y_step
            points.append([x, y])
        return points

    @staticmethod
    def interpolatepolygon(polygon):
        "return polygon interpolated each 5 units (meters pour epsg27572)"
        #eventuellement ajouter un cleaning pour les points en double dans le polygone resultant
        result = []
        for i in range(len(polygon) - 1):
            temp = Planning.interpolate_segment(5, polygon[i], polygon[i + 1])
            result += temp
        result += Planning.interpolate_segment(5, polygon[-1], polygon[0])
        return result

    @staticmethod
    def create_circle(center, radius):
        "create a circle with points evenly spaced around the circumference"
        points = []
        circumference = 2 * radius * math.pi  # Calcul de la circonférence à partir du rayon
        num_points = int(
            circumference / 2)  # Calcul du nombre de points en divisant la circonférence par une petite valeur arbitraire
        for i in range(num_points):
            angle = 2 * math.pi * i / num_points  # Calcul de l'angle en radians
            x = center[0] + radius * math.cos(angle)  # Calcul de la coordonnée x
            y = center[1] + radius * math.sin(angle)  # Calcul de la coordonnée y
            points.append((x, y))
        return points

    @staticmethod
    def fonction(polygon):
        #ameliorer surface par determinant matriciel https://www.youtube.com/watch?v=8sKOgLk8nA8
        "return surface of polygon=[pt1,pt2,...,ptn]"
        surf = 0
        list_triangle = []  # listedetriangles
        listpoint = []  # listedepoints
        xxyy = []  # [[xmin,xmax],[ymin,ymax]]
        triangle = []  # triangle
        for i in range(len(polygon)):
            if i < 3:
                triangle.append(polygon[i])  # i<3
                listpoint.append(polygon[i])
                if i == 2:
                    list_triangle.append(triangle)
                    xxyy = Planning.minmaxt(list_triangle)
            # condition sur i>=3
            else:
                # verifier l[i] hors des polygones
                boolean = 1
                if (polygon[i][0] - xxyy[0][0] >= 0
                        and polygon[i][0] - xxyy[0][1] <= 0
                        and polygon[i][1] - xxyy[1][0] >= 0
                        and polygon[i][1] - xxyy[1][1] <= 0):
                    # hors xminxmaxyminymax:out dans xminxmax hors yminymax:out hors xminxmax dans yminymax:out
                    # dans xminxmax dans yminymax surf sommedestriangles=surftriangleprecedent->in surf sommedestriangles>surftriangleprecedent->out
                    if Planning.dedans(
                            polygon[i], list_triangle
                    ):  # dedans(pt,listetriangle) renvoie 1 si à l'interieur d'un des triangle
                        boolean = 0
                else:
                    pass

                if boolean != 0:
                    # creertriangle_pt1
                    triangle = [polygon[i]]
                    pt1 = Planning.indexdist(
                        polygon[i], listpoint
                    )  # liste des points triés selon distance à l[i]
                    ###ici le index gist en postgis accelererait probablement
                    triangle.append(pt1[0])

                    # creertriangle_pt2
                    for k in range(1, len(pt1)):
                        if Planning.secantlll(
                            [polygon[i], pt1[k]], pt1, k
                        ):  # si la ligne l[i]pt1[k] interesecte une des lignes pt1[0]autresptsdept1
                            pass
                        else:
                            triangle.append(pt1[k])
                            # print(t)
                            list_triangle.append(triangle)
                            xxyy = Planning.minmaxt(list_triangle)
                            listpoint.append(polygon[i])
                            break
        for j in list_triangle:
            surf += Planning.surf(j)
        return surf

    @staticmethod
    def minmaxt(list_triangle):
        "renvoie les xxyy=[]#[[xmin,xmax],[ymin,ymax]] de la liste de triangle"
        listx = []
        listy = []
        for i in list_triangle:
            listx.append(i[0][0])
            listx.append(i[1][0])
            listx.append(i[2][0])
            listy.append(i[0][1])
            listy.append(i[1][1])
            listy.append(i[2][1])
        xmin = min(listx)
        xmax = max(listx)
        ymin = min(listy)
        ymax = max(listy)
        return [[xmin, xmax], [ymin, ymax]]

    @staticmethod
    def secantlll(xy1, lpt, k):
        "renvoie 1 si xy1 et les lignes de lpt sont secantes sauf la kieme"
        for i in range(1, len(lpt)):
            if i == k:
                pass
            if Planning.secant(xy1, [lpt[0], lpt[i]]):
                return 1

    #@staticmethod
    #def secant(xy1, xy2):
    #    "return 1 si xy1 et xy2 sont secants par determinant matriciel"
    #    # xy[[x1,y1],[x'1,y'1]]
    #    # si colinéaire det=0
    #    x1min = min(xy1[0][0], xy1[1][0])
    #    x1max = max(xy1[0][0], xy1[1][0])
    #    y1min = min(xy1[0][1], xy1[1][1])
    #    y1max = max(xy1[0][1], xy1[1][1])
    #    x2min = min(xy2[0][0], xy2[1][0])
    #    x2max = max(xy2[0][0], xy2[1][0])
    #    y2min = min(xy2[0][1], xy2[1][1])
    #    y2max = max(xy2[0][1], xy2[1][1])
    #    xmax = max(x1max, x2max)
    #    xmin = min(x1min, x2min)
    #    if (Planning.valeursabs((xy1[1][0] - xy1[0][0]) *
    #                            (xy2[1][1] - xy2[0][1]) -
    #                            (xy1[1][1] - xy1[0][1]) *
    #                            (xy2[1][0] - xy2[0][0])) == 0
    #        ):  ##etonnant, pas ==0 sur un cas a1=a2
    #        return 0
    #    if ((x1max < x2min) or
    #        (x1min > x2max)) and ((y1max < y2min) or
    #                              (y1min > y2max)):  # condition sur disjoints
    #        return 0
    #    else:  # equations ax+b
    #eviter division par zero pour segment vertical xy1
    #        if (xy1[1][0] - xy1[0][0]) == 0 and (xy2[1][0] - xy2[0][0]) != 0:
    #            a2 = (xy2[1][1] - xy2[0][1]) / (xy2[1][0] - xy2[0][0])
    #            b2 = xy2[1][1] - a2 * (xy2[1][0])
    #            y2 = a2 * xy1[1][0] + b2
    #            if y2 <= y1max and y2 >= y1min:
    #                return 1
    #            else:
    #                return 0
    #eviter division par zero pour segment vertical xy2
    #        elif (xy2[1][0] - xy2[0][0]) == 0 and (xy1[1][0] - xy1[0][0]) != 0:
    #            a1 = (xy1[1][1] - xy1[0][1]) / (xy1[1][0] - xy1[0][0])
    #            b1 = xy1[1][1] - a1 * (xy1[1][0])
    #            y1 = a1 * xy2[1][0] + b1
    #            if y1 <= y2max and y1 >= y2min:
    #                return 1
    #            else:
    #                return 0
    #cas deux segment verticaux
    #        elif (xy2[1][0] - xy2[0][0]) == 0 and (xy1[1][0] - xy1[0][0]) == 0:
    #            if y1min <= y2max or y2min<=y1max:
    #                return 1
    #            else:
    #                return 0
    #        else:
    #a1 pente xy1
    #            a1 = (xy1[1][1] - xy1[0][1]) / (xy1[1][0] - xy1[0][0])
    #b1 calculé pr trouver l'équation de la droite ax+b correspondant au segment
    #            b1 = xy1[1][1] - a1 * (xy1[1][0])
    #            a2 = (xy2[1][1] - xy2[0][1]) / (xy2[1][0] - xy2[0][0])
    #            b2 = xy2[1][1] - a2 * (xy2[1][0])
    #            if ((b2 - b1) / (a1 - a2)) <= xmax and ((b2 - b1) /
    #                                                    (a1 - a2)) >= xmin:
    #                return 1
    @staticmethod
    def secant(segment1, segment2):
        # segment1 et segment2 doivent être de la forme ((x1, y1), (x2, y2))

        # Calcul des valeurs des équations des droites pour chaque segment
        a1 = segment1[1][1] - segment1[0][1]
        b1 = segment1[0][0] - segment1[1][0]
        c1 = a1 * segment1[0][0] + b1 * segment1[0][1]

        a2 = segment2[1][1] - segment2[0][1]
        b2 = segment2[0][0] - segment2[1][0]
        c2 = a2 * segment2[0][0] + b2 * segment2[0][1]

        # Calcul du dénominateur de Cramer
        det = a1 * b2 - a2 * b1

        # Vérification si les segments sont parallèles
        if det == 0:
            return False

        # Calcul des coordonnées du point d'intersection
        x = (b2 * c1 - b1 * c2) / det
        y = (a1 * c2 - a2 * c1) / det

        # Vérification si le point d'intersection se trouve sur les deux segments
        if ((x >= segment1[0][0] and x <= segment1[1][0]) or (x >= segment1[1][0] and x <= segment1[0][0])) \
                and ((y >= segment1[0][1] and y <= segment1[1][1]) or (y >= segment1[1][1] and y <= segment1[0][1])) \
                and ((x >= segment2[0][0] and x <= segment2[1][0]) or (x >= segment2[1][0] and x <= segment2[0][0])) \
                and ((y >= segment2[0][1] and y <= segment2[1][1]) or (y >= segment2[1][1] and y <= segment2[0][1])):
            return True
        else:
            return False

