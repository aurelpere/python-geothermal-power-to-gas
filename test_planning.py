#!/usr/bin/python3
# coding: utf-8
"""
this is test_planning.py
"""
from planning import Planning
#visualisation @ https://www.geogebra.org/graphing


def test_isdigitpoint():
    "test of isdigitpoint function"
    planning = Planning()
    assert planning.isdigitpoint('8') == 1
    assert planning.isdigitpoint('.') == 1
    assert planning.isdigitpoint('-') == 1
    assert planning.isdigitpoint('a') == 0


def test_deccoord():
    "test of deccoord function"
    planning = Planning()
    assert planning.deccoord('POINT (290225.2699306086 1843146.0906546437)'
                             ) == ((290225.2699306086, 1843146.0906546437), )
    assert planning.deccoord(
        'POLYGON ((510786.4962177546 1817685.9314593724, 510790.71072088345 1817685.3160891854, 510791.9470301377 1817692.4214629498, 510787.81372920994 1817693.035697252, 510786.4962177546 1817685.9314593724))'
    ) == ((510786.4962177546, 1817685.9314593724),
          (510790.71072088345, 1817685.3160891854), (510791.9470301377,
                                                     1817692.4214629498),
          (510787.81372920994, 1817693.035697252), (510786.4962177546,
                                                    1817685.9314593724))
    assert planning.deccoord(
        'LINESTRING (391234.6474115489 1859681.5432677385, 391197.1101810548 1859520.811951668)'
    ) == ((391234.6474115489, 1859681.5432677385), (391197.1101810548,
                                                    1859520.811951668))


def test_valeursabs():
    "test of valeursabs function"
    planning = Planning()
    assert planning.valeursabs(-1) == 1
    assert planning.valeursabs(-20) == 20


def test_distancepoint():
    "test of distancepoint function"
    planning = Planning()
    assert planning.distancepoint(((1, 1), (2, 2))) == 1.4142135623730951


def test_centroide():
    "test of centroide function"
    planning = Planning()
    assert planning.centroide(((0, 0), (0, 1), (1, 1), (1, 0))) == (0.5, 0.5)


def test_surf():
    "test of surf function"
    planning = Planning()
    assert planning.surf([[1, 1], [1.5, 2], [2, 1]]) == 0.5


def test_dedans():
    "test of dedans function"
    planning = Planning()
    assert planning.dedans([1.2, 1.2], [[[1, 1], [1.5, 2], [2, 1]]]) == 1


def test_indexdist():
    "test of indexdist function"
    planning = Planning()
    assert planning.indexdist(
        [0, 1], [[0, 0], [0.5, 0], [1, 0], [1, 1], [2, 2]]) == [[0, 0], [1, 1],
                                                                [0.5, 0],
                                                                [1, 0], [2, 2]]


def test_fonction():
    "test of fonction function"
    planning = Planning()
    assert planning.fonction([[0, 2], [1, 1], [2, 2]]) == 1.0


def test_minmaxt():
    "test of minmaxt function"
    planning = Planning()
    assert planning.minmaxt([[[0, 2], [1, 1], [2, 2]], [[0, 1], [4, 4],
                                                        [0, 3]]]) == [[0, 4],
                                                                      [1, 4]]


def test_secantlll():
    "test of secantlll function"
    planning = Planning()
    assert planning.secantlll([[1, 1], [5, 5]], [[0, 1], [4, 4], [0, 3]],
                              3) == 1


def test_secant():
    "test of secant function"
    planning = Planning()
    assert planning.secant([[1, 1], [5, 5]], [[0, 1], [6, 1]]) == 1
    assert planning.secant([[1, 1], [5, 5]], [[0, 1], [6, 7]]) == 0
    assert planning.secant([[1.2, 1.8], [1.32, 0.72]],
                           [[2.34, 2.54], (1.4933333333333334, 1.32)]) == 0


def test_inside():
    "test of inside function"
    #visualisation @ https://www.geogebra.org/graphing
    planning = Planning()
    assert planning.inside([2.34, 2.54],
                           [[1.2, 1.8], [1.32, 0.72], [1.96, 1.44]]) == 0
    assert planning.inside(
        [1.24, 2.59], [[1.2, 1.8], [1.32, 0.72], [1.96, 1.44], [2.34, 2.54],
                       [0.64, 2.86], [0.58, 2.24], [1.43, 2.23]]) == 1
    assert planning.inside(
        [1.24, 3.25], [[1.2, 1.8], [1.32, 0.72], [1.96, 1.44], [2.34, 2.54],
                       [0.64, 2.86], [0.58, 2.24], [1.43, 2.23]]) == 0


def test_polygon():
    "test of polygon function"
    planning = Planning()
    assert planning.polygon(
        [[2.34, 2.54], [1.2, 1.8], [1.32, 0.72], [1.96, 1.44]],
        [[1.49, 1.32], [1.32, 2.17], [0.53, 1.83], [0.65, 1.1]]) == [[
            1.2, 1.8
        ], [1.49, 1.32]]
    #interpolation necessaire


def test_interpolate_segment():
    planning = Planning()
    assert planning.interpolate_segment(
        5, [0, 0], [100, 100]) == [[0.0, 0.0],
                                   [3.5714285714285716, 3.5714285714285716],
                                   [7.142857142857143, 7.142857142857143],
                                   [10.714285714285715, 10.714285714285715],
                                   [14.285714285714286, 14.285714285714286],
                                   [17.857142857142858, 17.857142857142858],
                                   [21.42857142857143, 21.42857142857143],
                                   [25.0, 25.0],
                                   [28.571428571428573, 28.571428571428573],
                                   [32.142857142857146, 32.142857142857146],
                                   [35.714285714285715, 35.714285714285715],
                                   [39.285714285714285, 39.285714285714285],
                                   [42.85714285714286, 42.85714285714286],
                                   [46.42857142857143, 46.42857142857143],
                                   [50.0, 50.0],
                                   [53.57142857142858, 53.57142857142858],
                                   [57.142857142857146, 57.142857142857146],
                                   [60.714285714285715, 60.714285714285715],
                                   [64.28571428571429, 64.28571428571429],
                                   [67.85714285714286, 67.85714285714286],
                                   [71.42857142857143, 71.42857142857143],
                                   [75.0, 75.0],
                                   [78.57142857142857, 78.57142857142857],
                                   [82.14285714285715, 82.14285714285715],
                                   [85.71428571428572, 85.71428571428572],
                                   [89.28571428571429, 89.28571428571429],
                                   [92.85714285714286, 92.85714285714286],
                                   [96.42857142857143, 96.42857142857143],
                                   [100.0, 100.0]]


def test_interpolatepolygon():
    planning = Planning()
    assert planning.interpolatepolygon([[0, 0],
                                        [10, 10], [10, 0]]) == [[0.0, 0.0],
                                                                [5.0, 5.0],
                                                                [10.0, 10.0],
                                                                [10.0, 10.0],
                                                                [10.0, 5.0],
                                                                [10.0, 0.0],
                                                                [10.0, 0.0],
                                                                [5.0, 0.0],
                                                                [0.0, 0.0]]

def test_createcircle():
    planning = Planning()
    assert planning.create_circle([0, 0],1)==[(1.0, 0.0), (-0.4999999999999998, 0.8660254037844387), (-0.5000000000000004, -0.8660254037844384)]
